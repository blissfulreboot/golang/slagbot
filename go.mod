module slagbot

go 1.19

require (
	github.com/sirupsen/logrus v1.9.0
	github.com/slack-go/slack v0.11.2
	gitlab.com/blissfulreboot/golang/conffee v1.0.1
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
